//
// Created by carlgonz 2020-02-24
//

#ifndef SUCHAI_FLIGHT_SOFTWARE_TASKTEST_H
#define SUCHAI_FLIGHT_SOFTWARE_TASKTEST_H

#include <math.h>

#include "suchai/config.h"

#include "suchai/osDelay.h"
#include "suchai/repoCommand.h"

#include "suchai/variables.h"

void taskTest(void* param);

#endif //SUCHAI_FLIGHT_SOFTWARE_TASKTEST_H
