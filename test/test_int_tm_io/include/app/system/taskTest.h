//
// Created by lurrea on 27-10-21.
//

#include "suchai/config.h"
#include "suchai/osThread.h"
#include "suchai/osDelay.h"
#include "suchai/repoCommand.h"

#include "suchai/math_utils.h"
#include <assert.h>
#include <string.h>


void taskTest(void* param);